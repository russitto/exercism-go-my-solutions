package reverse

import (
	"strings"
)

func String(inp string) string {
	spl := strings.Split(inp, "")
	out := ""
	for i := len(spl) - 1; i >= 0; i-- {
		out += spl[i]
	}
	return out
}
