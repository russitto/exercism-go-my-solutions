package space

type Planet string

const Y_EARTH = 31557600.0

var planets = map[Planet]float64{
	"Earth":   Y_EARTH,
	"Mercury": Y_EARTH * 0.2408467,
	"Venus":   Y_EARTH * 0.61519726,
	"Mars":    Y_EARTH * 1.8808158,
	"Jupiter": Y_EARTH * 11.862615,
	"Saturn":  Y_EARTH * 29.447498,
	"Uranus":  Y_EARTH * 84.016846,
	"Neptune": Y_EARTH * 164.79132,
}

func Age(seconds float64, plan Planet) float64 {
	return seconds / planets[plan]
}
