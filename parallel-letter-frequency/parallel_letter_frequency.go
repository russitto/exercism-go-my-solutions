package letter

func FreqChan(inp string, out chan FreqMap) {
	out <- Frequency(inp)
}

func ConcurrentFrequency(strs []string) FreqMap {
	out := make(FreqMap)
	for i := 0; i < len(strs); i++ {
		frchan := make(chan FreqMap)
		go FreqChan(strs[i], frchan)
		fr := <-frchan
		for k, v := range fr {
			out[k] += v
		}
	}
	return out
}
