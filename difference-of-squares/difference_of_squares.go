package diffsquares

func SquareOfSums(num int) int {
	return pow2((num * (num + 1)) / 2)
}

func SumOfSquares(num int) int {
	return (num * (num + 1) * (num * 2 + 1)) / 6
}

func Difference(num int) int {
	return SquareOfSums(num) - SumOfSquares(num)
}

func pow2(num int) int {
	return num * num
}
