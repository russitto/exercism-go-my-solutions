package bob

import "strings"

const M_FINE = "Fine. Be that way!"
const M_WHATEVER = "Whatever."
const M_CHILLOUT = "Whoa, chill out!"
const M_SURE = "Sure."
const M_CALM = "Calm down, I know what I'm doing!"

// Hey should have a comment documenting it.
func Hey(remark string) string {
	remark = strings.TrimSpace(remark)

	if remark == "" {
		return M_FINE
	}

	isAnswer := strings.HasSuffix(remark, "?")
	isCaps := remark == strings.ToUpper(remark)
	isNoCaps := remark == strings.ToLower(remark)

	if isCaps && !isNoCaps {
		if isAnswer {
			return M_CALM
		}
		return M_CHILLOUT
	}

	if isAnswer {
		return M_SURE
	}

	return M_WHATEVER
}
